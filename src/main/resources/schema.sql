

DROP TABLE IF EXISTS graphs;
CREATE TABLE graphs (
 id INT NOT NULL AUTO_INCREMENT,
name VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (id)
);

DROP TABLE IF EXISTS vertices;
CREATE TABLE vertices (
 id INT NOT NULL AUTO_INCREMENT,
 data VARCHAR( 255 ) NOT NULL ,
 graphid INT,
 PRIMARY KEY ( id ),
 FOREIGN KEY (graphid) REFERENCES graphs(id)
);

DROP TABLE IF EXISTS edges;
CREATE TABLE edges(
 id INT NOT NULL AUTO_INCREMENT,
 weight INTEGER( 255 ) NOT NULL ,
 sourcevertexid INT,
 destinationvertexid INT,
 PRIMARY KEY (id),
 FOREIGN KEY (sourcevertexid) REFERENCES vertices(id),
 FOREIGN KEY (destinationvertexid) REFERENCES vertices(id)
);
