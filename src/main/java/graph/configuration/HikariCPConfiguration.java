package graph.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HikariCPConfiguration {
    @Value("${graph.database.driver}")
    private String driver;

    @Value("${graph.database.host}")
    private String host;

    @Value("${graph.database.port}")
    private Integer port;

    @Value("${graph.database.name}")
    private String database;

    @Value("${graph.database.user}")
    private String user;

    @Value("${graph.database.pass}")
    private String password;

    @Value("${graph.database.maximalPoolSize}")
    private Integer maximalPoolSize;

    @Bean(name = "source", destroyMethod = "close")
    public HikariDataSource dataSource() {
        final HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database + "?useUnicode=true&characterEncoding=UTF-8&useSSL=false");
        configuration.setDriverClassName(driver);
        configuration.setUsername(user);
        configuration.setPassword(password);
        configuration.setMaximumPoolSize(maximalPoolSize);

        return new HikariDataSource(configuration);
    }
}
