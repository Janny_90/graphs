package graph.model;


public class Edge {
    public Integer id;
    public Integer sourcevertexid;
    public Integer destinationvertexid;
    public Integer weight;
}
