package graph.mappers;

import graph.model.Graph;
import org.apache.ibatis.annotations.*;
@Mapper
public interface GraphMapper {
    @Select("SELECT id, name from graphs WHERE id = #{id}")
    Graph select(int id);

    @Insert("INSERT into graphs(name) VALUES(#{graphName})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertGraph(Graph graph);

    @Update("UPDATE graphs SET name=#{graphName} WHERE id =#{graphId}")
    void update(Graph graph);

    @Delete("DELETE FROM graphs WHERE id =#{graphId}")
    void delete(int id);
}
