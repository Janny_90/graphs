package graph.mappers;

import graph.model.Edge;
import org.apache.ibatis.annotations.*;

@Mapper
public interface EdgeMapper {
    @Select("SELECT id, sourcevertexid,destinationvertexid,weight  from edges WHERE id = #{id}")
    Edge select(int id);

    @Insert("INSERT into edges(sourcevertexid,destinationvertexid,weight) VALUES(#{sourcevertexid},#{destinationvertexid},#{weight})")//todo
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insert(Edge edge);

    @Update("UPDATE edges SET sourcevertexid =#{sourcevertexid}, destinationvertexid =#{destinationvertexid},weight=#{weight}  WHERE id =#{id}")
    void update(Edge edge);

    @Delete("DELETE FROM edges WHERE id =#{id}")
    void delete(int id);
}
