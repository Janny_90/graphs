package graph.services.implementation;

import graph.mappers.EdgeMapper;
import graph.model.Edge;
import graph.services.interfaces.EdgeService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EdgeServiceImplementation implements EdgeService {
    private final EdgeMapper edgeMapper;

    @Override
    public int create(Edge entity) {
        return edgeMapper.insert(entity);
    }

    @Override
    public Edge find(int id) {
        return edgeMapper.select(id);
    }

    @Override
    public void update(Edge entity) {
        edgeMapper.update(entity);
    }

    @Override
    public void delete(int id) {
        edgeMapper.delete(id);
    }
}
