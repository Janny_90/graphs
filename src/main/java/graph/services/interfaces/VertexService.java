package graph.services.interfaces;

import graph.model.Vertex;

public interface VertexService extends CRUDService<Vertex>{
}
