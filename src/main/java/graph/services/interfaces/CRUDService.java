package graph.services.interfaces;

public interface CRUDService<T> {
    int create(T entity);

    T find(int id);

    void update(T entity);

    void delete(int id);
}
